from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from entries import views

urlpatterns = [
    path('', views.EntryList.as_view()),
    path('<int:pk>/', views.EntryDetail.as_view()),
    path('<str:word>/', views.EntryQuery.as_view()),
    path('<str:word>/neighbors', views.EntryNeighbors.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
