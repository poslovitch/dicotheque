from django.core.management.base import BaseCommand
from lxml.etree import iterparse, QName
from bz2file import BZ2File
from urllib.request import urlretrieve
from dictionaries.models import Dictionary
from entries.models import Entry

DENYLIST = [
    'Texte entier',
    'Index alphabétique - A',
    'Index alphabétique - B',
    'Index alphabétique - C',
    'Index alphabétique - D',
    'Index alphabétique - E',
    'Index alphabétique - F',
    'Index alphabétique - G',
    'Index alphabétique - H',
    'Index alphabétique - I',
    'Index alphabétique - IJ',
    'Index alphabétique - J',
    'Index alphabétique - K',
    'Index alphabétique - L',
    'Index alphabétique - M',
    'Index alphabétique - N',
    'Index alphabétique - O',
    'Index alphabétique - P',
    'Index alphabétique - Q',
    'Index alphabétique - R',
    'Index alphabétique - S',
    'Index alphabétique - T',
    'Index alphabétique - U',
    'Index alphabétique - V',
    'Index alphabétique - W',
    'Index alphabétique - X',
    'Index alphabétique - Y',
    'Index alphabétique - Z',
    'Lettre A',
    'Lettre B',
    'Lettre C',
    'Lettre D',
    'Lettre E',
    'Lettre F',
    'Lettre G',
    'Lettre H',
    'Lettre I',
    'Lettre IJ',
    'Lettre J',
    'Lettre K',
    'Lettre L',
    'Lettre M',
    'Lettre N',
    'Lettre O',
    'Lettre P',
    'Lettre Q',
    'Lettre R',
    'Lettre S',
    'Lettre T',
    'Lettre U',
    'Lettre V',
    'Lettre W',
    'Lettre X',
    'Lettre Y',
    'Lettre Z',
]

# Dictionaries whose entries can be safely split if ', ou ' is present
SPLIT_ENTRIES = {
    'Q24230258': ', ou ', # L'Encyclopédie
    'Q19164883': ', ou ', # portatif de cuisine, ...
    'Q54099419': ', ou ', # Trévoux
    'Q19151100': ', ou ', # agriculture (Rozier?)
    'Q131156163': ', ',   # Petit dictionnaire de la langue fr. (1836)
    'Q131289747': ', ',   # Petit dictionnaire de la langue fr. (1860)
}

class Command(BaseCommand):
    help = "Updates entries of each dictionary available based on the latest Wikisource dump"

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        # Use https://pypi.org/project/mwsql to handle the pagelinks sql dump

        # Download the pages-meta-current dump
        print("Downloading latest pages-meta-current dump")
        url = 'https://dumps.wikimedia.org/frwikisource/latest/frwikisource-latest-pages-articles-multistream.xml.bz2'
        path, _ = urlretrieve(url,reporthook=download_report_hook)
        print()
        print("Download complete")

        # Drop all existing entries
        print('Dropping all existing entries')
        deleted, _ = Entry.objects.all().delete()
        print(f'Deleted {deleted} entries.')

        # Get all available dictionaries
        dictionaries = {}
        for dictionary in Dictionary.objects.all():
            dictionaries[dictionary.frwikisource] = dictionary

        count = 0
        for page in read_pages_dump(path, process_function=process_page):
            (page_id, title) = page
            if '/' not in title:
                continue

            prefix, word = title.rsplit('/', 1)
            if word in DENYLIST:
                continue

            if prefix in dictionaries:
                dictionary = dictionaries[prefix]
                words = [word]
                # Some dictionaries have multiple entries merged into one
                # e.g. 'WARNETON, ou VARNETON' ; 'GLORIEUX, GLORIEUSE'
                if dictionary.qid in SPLIT_ENTRIES.keys():
                    words = word.split(SPLIT_ENTRIES[dictionary.qid])

                for word in words:
                    e = Entry(dictionary=dictionary, word=word, word_normalized=word.lower(), page_title=title, page_id=page_id)
                    try:
                        e.save()
                        count += 1
                        if count % 1000 == 0:
                            print(f'Processed {count} entries')
                    except Exception as e:
                        print(f'ERROR: {title} for dictionary {prefix}')
                        print(e)
        
        print(f'DONE: Processed {count} entries')


def read_pages_dump(path, process_function):
    with BZ2File(path) as xml_file:
        context = iterparse(xml_file, events=("start", "end"))
        
        # From Stackoverflow
        # https://stackoverflow.com/questions/9856163/using-lxml-and-iterparse-to-parse-a-big-1gb-xml-file
        _, root = next(context)
        start_tag = None
        for event, element in context:
            if event == 'start' and start_tag is None:
                start_tag = element.tag
            if event == 'end' and element.tag == start_tag:
                processed_element = process_function(element) # This is where we actually process the element
                if processed_element is not None:
                    yield processed_element
                start_tag = None
                root.clear() # Clear the root everytime we finish processing an element to keep memory usage low


def process_page(element):
    if not QName(element).localname == 'page': 
        return
    
    # Filter out any page that isn't in the main namespace
    namespace = element.find('{*}ns').text
    if namespace != '0':
        return

    # for reference, this one is the actual article text: element.xpath('./revision/text/text()')
    return (element.find('{*}id').text, element.find('{*}title').text) 


def download_report_hook(count, block_size, total_size):
    if count % 1024 == 0:
        print(f'{count} [{count * block_size / total_size * 100:.2f} %]', end='\r')
