from django.db import models


class Entry(models.Model):
    dictionary = models.ForeignKey("dictionaries.Dictionary", on_delete=models.CASCADE)
    word = models.CharField(max_length=256)
    word_normalized = models.CharField(max_length=256)
    page_id = models.PositiveIntegerField()
    page_title = models.CharField(max_length=512)
    # TODO: stop removing all the entries when updating, but add a "last-seen-in 2024-XX-XX" ?
