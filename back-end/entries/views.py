from entries.models import Entry
from entries.serializers import EntrySerializer
from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.response import Response


class EntryList(generics.ListAPIView):
    queryset = Entry.objects.all()
    serializer_class = EntrySerializer


class EntryDetail(generics.RetrieveAPIView):
    queryset = Entry.objects.all()
    serializer_class = EntrySerializer


class EntryQuery(APIView):
    def get(self, request, word, format=None):
        entries = Entry.objects.filter(word_normalized__icontains=word)
        serializer = EntrySerializer(entries, many=True)
        return Response(serializer.data)


class EntryNeighbors(APIView):
    def get(self, request, word, format=None):
        all_entries = [neighbor['word_normalized'] for neighbor in Entry.objects.values('word_normalized').distinct().order_by('word_normalized')]
        try:
            word_index = all_entries.index(word.lower())
            return Response(all_entries[max(0, word_index-10):min(len(all_entries), word_index+11)])
        except ValueError:
            # this word is not in the list
            return Response([])
