from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from dictionaries import views

urlpatterns = [
    path('', views.DictionaryList.as_view()),
    path('<int:pk>/', views.DictionaryDetail.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
