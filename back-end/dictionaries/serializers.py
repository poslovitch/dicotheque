from rest_framework import serializers
from dictionaries.models import Dictionary, Work

class WorkSerializer(serializers.ModelSerializer):
    class Meta:
        model = Work
        fields = "__all__"


class DictionarySerializer(serializers.ModelSerializer):
    edition_of = WorkSerializer()

    class Meta:
        model = Dictionary
        fields = "__all__"
