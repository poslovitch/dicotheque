from dictionaries.models import Dictionary
from dictionaries.serializers import DictionarySerializer
from rest_framework import generics


class DictionaryList(generics.ListAPIView):
    queryset = Dictionary.objects.all()
    serializer_class = DictionarySerializer


class DictionaryDetail(generics.RetrieveAPIView):
    queryset = Dictionary.objects.all()
    serializer_class = DictionarySerializer
