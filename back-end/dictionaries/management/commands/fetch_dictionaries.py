from django.core.management.base import BaseCommand
from django.conf import settings
from dictionaries.models import Dictionary, Work
import requests

class Command(BaseCommand):
    help = "Fetches data about each dictionary that's in the settings"

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        for qid in settings.DATA_DICTIONARIES:
            print(qid)

            if Dictionary.objects.filter(qid=qid).exists():
                dictionary = Dictionary.objects.get(qid=qid)
            else:
                dictionary = Dictionary(qid=qid)

            response = requests.get(f'https://www.wikidata.org/wiki/Special:EntityData/{qid}.json')
            json = response.json()['entities'][qid]

            dictionary.title = json['claims']['P1476'][0]['mainsnak']['datavalue']['value']['text']
            dictionary.frwikisource = json['sitelinks']["frwikisource"]['title']

            # progress badge
            # (we use a for loop here to get only the first one if it exists or skip otherwise)
            for badge in json['sitelinks']['frwikisource']['badges']:
                if badge == 'Q20748093':
                    dictionary.wikisource_progress = dictionary.WikisourceProgress.VALIDATED
                elif badge == 'Q20748092':
                    dictionary.wikisource_progress = dictionary.WikisourceProgress.PROOFREAD
                elif badge == 'Q20748091':
                    dictionary.wikisource_progress = dictionary.WikisourceProgress.NOT_PROOFREAD
                else:
                    dictionary.wikisource_progress = dictionary.WikisourceProgress.UNKNOWN_SITELINK_BADGE
                break

            # date
            date = ''
            if 'P577' in json['claims']:
                p577 = json['claims']['P577']
                d = p577[0]
                if d['mainsnak']['snaktype'] == 'value':
                    date = d['mainsnak']['datavalue']['value']['time']
                    date = date[1:5] # only keep the year
            
            dictionary.date = date

            # authors
            authors = []
            p50 = json['claims']['P50']
            for author in p50:
                if author['mainsnak']['snaktype'] == 'value':
                    authors.append(author['mainsnak']['datavalue']['value']['id'])
                elif author['mainsnak']['snaktype'] == 'somevalue':
                    authors.append('@anonymous')
            dictionary.authors = authors

            # publication_places
            publication_places = []
            if 'P291' in json['claims']:
                p291 = json['claims']['P291']
                for publication_place in p291:
                    publication_places.append(publication_place['mainsnak']['datavalue']['value']['id'])
            dictionary.publication_places = publication_places

            # publishers
            publishers = []
            if 'P123' in json['claims']:
                p123 = json['claims']['P123']
                for publisher in p123:
                    if publisher['mainsnak']['snaktype'] == 'value': # TODO: handle == somevalue (Q121502026)
                        publishers.append(publisher['mainsnak']['datavalue']['value']['id'])
            dictionary.publishers = publishers

            # languages
            languages = []
            p407 = json['claims']['P407']
            for language in p407:
                languages.append(language['mainsnak']['datavalue']['value']['id'])
            dictionary.languages = languages
            
            # EDITION OF
            if 'P629' in json['claims']:
                edition_of = json['claims']['P629'][0]['mainsnak']['datavalue']['value']['id']
                try:
                    work = Work.objects.get(qid=edition_of)
                except Work.DoesNotExist:
                    work = Work(qid=edition_of)
                
                work_response = requests.get(f'https://www.wikidata.org/wiki/Special:EntityData/{edition_of}.json')
                work_json = work_response.json()['entities'][edition_of]

                # Subjects
                subjects = []
                if 'P921' in work_json['claims']:
                    p921 = work_json['claims']['P921']
                    for subject in p921:
                        subjects.append(subject['mainsnak']['datavalue']['value']['id'])
                work.subjects = subjects
                
                # Genres
                genres = []
                if 'P136' in work_json['claims']:
                    p136 = work_json['claims']['P136']
                    for genre in p136:
                        genre_qid = genre['mainsnak']['datavalue']['value']['id']
                        if genre_qid in settings.GENRES_ALLOWLIST:
                            genres.append(genre_qid)
                work.genres = genres

                work.save()
                dictionary.edition_of = work

            dictionary.save()
