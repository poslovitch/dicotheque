from django.contrib import admin
from dictionaries.models import Dictionary, Work


class DictionaryAdmin(admin.ModelAdmin):
    pass


class WorkAdmin(admin.ModelAdmin):
    pass


admin.site.register(Dictionary, DictionaryAdmin)
admin.site.register(Work, WorkAdmin)
