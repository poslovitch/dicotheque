from django.db import models

class Dictionary(models.Model):
    qid = models.CharField(max_length=24, unique=True) # Qid of the edition
    title = models.CharField(max_length=512) # Title of the edition
    frwikisource = models.CharField(max_length=512) # Prefix of all entries of this dictionary on fr.wikisource.org
    date = models.CharField(max_length=24, blank=True) # Publication date
    authors = models.JSONField(default=list, blank=True)
    publication_places = models.JSONField(default=list, blank=True)
    publishers = models.JSONField(default=list, blank=True)
    languages = models.JSONField(default=list, blank=True)
    edition_of = models.ForeignKey("dictionaries.Work", null=True, on_delete=models.CASCADE)

    class WikisourceProgress(models.TextChoices):
        VALIDATED = 'validated'
        PROOFREAD = 'proofread'
        NOT_PROOFREAD = 'not_proofread'
        UNKNOWN = 'unknown'
        UNKNOWN_SITELINK_BADGE = 'unknown_sitelink_badge'

    wikisource_progress = models.CharField(max_length=32, choices=WikisourceProgress.choices, default=WikisourceProgress.UNKNOWN)

class Work(models.Model):
    qid = models.CharField(max_length=24)
    subjects = models.JSONField(default=list, blank=True)
    genres = models.JSONField(default=list, blank=True)
