from django.db.models import Avg, Count, F, OuterRef, Q, Subquery, Sum
from django.db.models.functions import Round
from rest_framework.views import APIView
from rest_framework.response import Response
from dictionaries.models import Dictionary
from entries.models import Entry
import string

class Stats(APIView):
    def get(self, request, format=None):
        dictionary_per_unique_written_form = Entry.objects.values('word_normalized').distinct().annotate(dictionary_count=Count("dictionary"))

        # print(
        #     Entry.objects.values('word_normalized').annotate(n=Count("dictionary")).values("n").annotate(entries_count=Subquery(
        #         Entry.objects.values('word_normalized').annotate(dictionary_count=Count("dictionary")).filter(dictionary_count=OuterRef('n')).annotate(count=Count('pk')).values('count')
        #     )).order_by("n")
        # )

        letters = list(string.ascii_lowercase)
        entries_per_starting_letter = {}
        for letter in letters:
            entries_per_starting_letter[letter] = Entry.objects.filter(word_normalized__istartswith=letter).distinct().count()

        data = {
            "dictionaries_count": Dictionary.objects.count(),
            "entries_count": Entry.objects.count(),
            "unique_written_forms_count": Entry.objects.values('word_normalized').distinct().count(),
            "dictionary_per_unique_written_form_average": dictionary_per_unique_written_form.aggregate(average=Round(Avg("dictionary_count"), precision=2))['average'],
            "entries_per_dictionary": Dictionary.objects.annotate(entries_count=Count("entry")).values('id', 'entries_count').order_by('-entries_count'),
            "entries_per_starting_letter": entries_per_starting_letter,
            # "unique_written_forms_in_exactly_n_dictionaries": dictionary_per_unique_written_form.values("dictionary_count").order_by("dictionary_count")
        }
        return Response(data)
