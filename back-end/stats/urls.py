from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from stats import views

urlpatterns = [
    path('', views.Stats.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
