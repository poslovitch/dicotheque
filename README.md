# Dicothèque

<img src="./front-end/assets/logo.svg" alt="Dicotheque logo" height=64/>

*dico* + *-thèque*, library of dictionaries.

## Description
The *Dicothèque* is a website aimed at providing a search engine for dictionary entries transcluded on Wikisource.
It facilitates comparing these entries in a corpus of dictionaries.

### Inception
The *Dicothèque* originates from an idea that was showcased by Lyokoï in WikiConvention Francophone 2022 in Paris.
The following documents can help you better understand what the Dicothèque is about, and why it was made:
* [Presentation in French](https://commons.wikimedia.org/wiki/File:Une_interface_pour_lire_tous_les_dictionnaires_dans_Wikisource_-_Lyoko%C3%AF.pdf)
* [Audio- and screen-recording of the live session in French](https://commons.wikimedia.org/wiki/File:WCF2022_-_Conf%C3%A9rence_-_Une_interface_pour_lire_tous_les_dictionnaires_dans_Wikisource.webm)

At WikidataCon 2023, Poslovitch held a talk showcasing an on-wiki working prototype of the Dicothèque.
[Try to find the video!]

The idea of an on-wiki interface has been scrapped in December 2023 in favor of a dedicated website.
This allowed to circumvent many restrictions pertaining to providing the Dicothèque as a Vue/JavaScript user script on the French Wikisource. 

### Comparison with similar projects

| Project                              | Available dictionaries | Side-by-side comparison | Text format | Content license    |
|--------------------------------------|------------------------|-------------------------|-------------|--------------------|
| **Dicothèque**                       | 39                     | ✅                      | ✅          | CC0 & CC-BY-SA 4.0 |
| Dictionnaire des Francophones        | ?                      | ✅                      | ✅          | Mostly "open data" |
| Portail lexical du CNRTL             | 7                      | ❌                      | ✅          | Public Domain      |
| Métadictionnaire médical multilingue | 60                     | ❌                      | ❌          | Licence ouverte    |

## Usage
The Dicothèque is available at [dicotheque.org](https://dicotheque.org).

## Support
Questions? Issues?
Please file them on the French Wikisource [Dicothèque's talk page](https://fr.wikisource.org/wiki/Discussion_Wikisource:Dicoth%C3%A8que).

## Contributing
Contributions are welcome, and especially contributions to improve:
* code quality
* UI/UX

### Useful commands
* `npm run dev`: launches the development server
* `npm run lint`: lint
* `npm run format`: formats the js files

### Requirements
* NodeJS 20.x

### Getting started
1. Clone the repository
2. Run `npm clean-install`

### Planned improvements
1. Add Unit Tests
2. Set up i18n

## Authors and acknowledgment
* [User:Poslovitch](https://meta.wikimedia.org/wiki/User:Poslovitch)
* [User:Lyokoï](https://meta.wikimedia.org/wiki/User:Lyoko%C3%AF)
* [User:Manjiro5](https://meta.wikimedia.org/wiki/User:Manjiro5) for the [awesome logo he made for the Dicothèque](https://commons.wikimedia.org/wiki/File:Dicoth%C3%A8que_logo.svg) (and released under CC0)!
