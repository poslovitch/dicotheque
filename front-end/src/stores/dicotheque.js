import { defineStore } from 'pinia'

export const useDicothequeStore = defineStore('dicotheque', {
  state: () => ({
    word: '',
    strictSearch: false,
    lastSearch: {
      word: '',
      strictSearch: false,
    },
    orderResults: 'chronological',
    filters: {
      languages: [
        {
          name: 'Non précisée',
          active: true,
          qid: undefined,
        },
      ],
      subjects: [
        {
          name: 'Non précisé',
          active: true,
          qid: undefined,
        },
      ],
      genres: [
        {
          name: 'Non précisé',
          active: true,
          qid: undefined,
        },
      ],
    },
    dictionaries: [],
    labels: {
      '@anonymous': 'Anonyme',
    },
  }),
  getters: {
    activeLanguagesFilters: (state) => state.filters.languages.filter((f) => f.active),
    activeSubjectsFilters: (state) => state.filters.subjects.filter((f) => f.active),
    activeGenresFilters: (state) => state.filters.genres.filter((f) => f.active),
  },
})
