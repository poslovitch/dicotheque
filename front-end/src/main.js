import { createApp } from 'vue'
import { createPinia } from 'pinia'
import { createWebHistory, createRouter } from 'vue-router'
import { CdxTooltip } from '@wikimedia/codex'

import App from './App.vue'
import HomeView from './views/HomeView.vue'
import AboutView from './views/AboutView.md'
import StatisticsView from './views/StatisticsView.vue'
import ContactView from './views/ContactView.md'

const routes = [
  { path: '/', component: HomeView },
  { path: '/about', component: AboutView },
  { path: '/statistics', component: StatisticsView },
  { path: '/contact', component: ContactView },
]

const router = createRouter({
  history: createWebHistory(),
  routes,
})

const Pinia = createPinia()

createApp(App).use(router).use(Pinia).directive('tooltip', CdxTooltip).mount('#app')
