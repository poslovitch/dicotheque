## À propos

La dicothèque est un projet lancé en 2022 par Florian Cuny (Poslovitch) et Lucas Lévêque
(Lyokoï), deux contributeurs au projets Wikimédias et passionnés par les dictionnaires.

### Pourquoi cet outil ?

Cet outil est issu d’un constat et d’un manque. Il existe plusieurs site internet qui sont des
silos de consultation de dictionnaires, mais aucun n’est vraiment complet dans la couverture
des dictionnaires ou bien est spécialisé. D’un autre côté le projet Wikisource où nous sommes
contributeurs dispose d’un grand nombre de dictionnaires informatisé, relu et sectionné dont
la consultation n’est possible qu’ouvrage par ouvrage. Et n’est pas aisé, surtout pour les
ouvrages de plus grande taille.

Nous avons donc décidé de créer cet outil qui récupère les contenus des dictionnaires relu par
la communauté de Wikisource et qui, avec les métadonnées, permet de consulter, filtrer, trier
et se promener dans les dictionnaires présents.

Wikisource étant un projet collaboratif et contributif qui permet de numériser et
d’informatiser des ouvrages. Des dictionnaires y sont relus régulièrement et viennent enrichir
la base de la Dicothèque au fur et à mesure du temps.

Nous n’avons pas de limite de nature des dictionnaires, qu’ils soient de langue,
encyclopédique, humoristique ou autres.

### Comment fonctionne-t-il ?

La Dicothèque utilise deux projets Wikimédia pour fonctionner.

Wikisource : Projet d’informatisation d’ouvrages du domaine public et en licence libre, on va
y chercher le contenu des dictionnaires lus et en cours de relecture. La Dicothèque vient y
chercher le contenu du dictionnaire. S’il y a une erreur de relecture, elle est corrigeable en
direct sur la page de relecture de Wikisource, et sera diffusé ici peu après.

Wikidata : Projet de base de connaissances, on va y chercher les métadonnées des dictionnaires
que nous affichons. Les information des deux colonnes des côtés de l’interface de la
Dicothèque y sont récupérées. Plus nous avons de données sur les dictionnaires plus nous
pouvons permettre au lectorat de trier et filtrer.

### Puis-je participer ?

Oui, vous pouvez informatiser et relire des dictionnaires sur Wikisource, aider à finir
d’informatiser ceux en cours, en commencer (et terminer, pourquoi pas !) des nouveaux, relire
ceux qui ont besoin de relecture. Cela nous permet d’augmenter qualitativement et
quantitativement le contenu de la Dicothèque. Nous vous revoyons sur la page d’aide spécifique :
[Aide:Éditer un Dictionnaire](https://fr.wikisource.org/wiki/Aide:%C3%89diter_un_Dictionnaire).

### Quelles sont les perspectives ?

La potentialité de l’outil est immense. Il existe plusieurs dizaines de milliers de
dictionnaires qui sont dans le domaine public. Nous gérons la variabilité au niveau de
l’édition et comptons bien intégrer toutes les éditions des dictionnaires du domaine public.

Actuellement, nous nous concentrons sur les dictionnaires en langue française, mais nous
pensons pouvoir intégrer d’autres langues de description dans le futur. Cela est surtout
dépendant de ce qui est fait en terme de structure des dictionnaires dans les autres versions
linguistiques de Wikisource.
