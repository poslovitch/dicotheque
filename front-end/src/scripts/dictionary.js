export const parseWikidataItemOfEdition = function (json) {
  const dictionary = {
    enabled: true,
    available: true,
    wikidata: json.qid,
    missing: [],
    subject: [undefined], // undefined means "non précisé", default value
    genre: [undefined],
    language: [undefined],
  }

  dictionary.id = json.id

  if (json.title) {
    // title
    dictionary.title = json.title
  } else {
    dictionary.missing.push('titre')
  }
  if (json.date) {
    // date de publication
    dictionary.date = json.date
  } else {
    dictionary.missing.push('date')
  }
  if (json.edition_of) {
    // edition de
    dictionary.work = json.edition_of.qid
    if (json.edition_of.subjects.length !== 0) {
      dictionary.subject = json.edition_of.subjects
    }
    if (json.edition_of.genres.length !== 0) {
      dictionary.genre = json.edition_of.genres
    }
  } else {
    dictionary.missing.push('édition ou traduction de')
  }
  if (json.languages && json.languages.length !== 0) {
    // langues
    dictionary.language = json.languages
  } else {
    dictionary.missing.push('langue')
  }
  if (json.authors && json.authors.length !== 0) {
    // auteurs/autrices
    dictionary.authors = json.authors
  } else {
    dictionary.missing.push('auteur ou autrice')
  }
  if (json.publication_places) {
    // lieu(x) de publication
    dictionary.publicationPlaces = json.publication_places
  }
  dictionary.publishers = json.publishers

  if (json.frwikisource) {
    // sitelink frwikisource
    dictionary.wikisource = json.frwikisource
  } else {
    dictionary.missing.push('lien wikisource')
  }

  if (json.wikisource_progress) {
    dictionary.wikisource_progress = json.wikisource_progress
  }

  return dictionary
}
