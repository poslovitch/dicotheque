import vuejsaccessibility from "eslint-plugin-vuejs-accessibility";
import eslint from '@eslint/js';
import vue from 'eslint-plugin-vue';
import { FlatCompat } from '@eslint/eslintrc'
import { dirname } from 'node:path'
import { fileURLToPath } from 'node:url'

const __filename = fileURLToPath(import.meta.url)
const __dirname = dirname(__filename)
const compat = new FlatCompat({ baseDirectory: __dirname })


export default [
    {
        ignores: ['**/dist/**']
    },
    eslint.configs.recommended,
    ...vue.configs["flat/recommended"],
    ...vuejsaccessibility.configs['flat/recommended'],
    ...compat.extends('@vue/eslint-config-prettier/skip-formatting'),
    {
        files: ['**/*.vue', '**/*.js'],
        rules: {
            'eqeqeq': 'error',
            'no-var': 'error',
            'prefer-const': 'error',
        },
        languageOptions: {
            ecmaVersion: 'latest'
        }
    }
];
  